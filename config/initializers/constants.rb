module Constants
  module DefaultSize
    MAX_SIMILAR_PRODUCTS          = 4
    MAX_POPULAR_CATEGORY_PRODUCTS = 3
    MIN_ORDER_QUANTITY            = 1
  end

  module SortType
    NEW_ARRIVAL = 0
    PRICE_ASC   = 1
    PRICE_DESC  = 2
    OLDDER      = 3
  end
end