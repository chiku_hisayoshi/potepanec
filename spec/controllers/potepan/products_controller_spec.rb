require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  shared_examples '正常レスポンスを返すこと' do
    it '200レスポンスを返すこと' do
      expect(response).to have_http_status '200'
    end
    it 'アクセスに成功すること' do
      expect(response).to be_successful
    end
  end

  shared_examples 'ルートへリダイレクトされること' do
    it 'ルートへリダイレクトされること' do
      expect(response).to redirect_to potepan_root_path
    end
  end

  shared_examples '302レスポンスを返すこと' do
    it '302レスポンスを返すこと' do
      expect(response).to have_http_status '302'
    end
  end

  describe 'GET #show' do
    let(:product) { create(:product) }
    context 'HTMLの場合' do
      before { get :show, params: { id: product.slug } }
      it_behaves_like '正常レスポンスを返すこと'
    end
    context 'Ajaxの場合' do
      before { get :show, xhr: true, params: { id: product.slug } }
      it_behaves_like '正常レスポンスを返すこと'
    end
  end

  describe 'GET #index' do
    context 'キーワードがあるとき' do
      before { get :index, params: { keyword: 'rails' } }
      it_behaves_like '正常レスポンスを返すこと'
    end

    context 'キーワードが空白のとき' do
      before { get :index, params: { keyword: '' } }
      it_behaves_like 'ルートへリダイレクトされること'
      it_behaves_like '302レスポンスを返すこと'
    end

    context 'キーワード自体送信していないとき' do
      before { get :index }
      it_behaves_like 'ルートへリダイレクトされること'
      it_behaves_like '302レスポンスを返すこと'
    end
  end
end
