require 'rails_helper'

RSpec.describe 'Products', type: :system do
  scenario '関連商品やバリエーション,在庫を持たない商品のページを操作する' do
    product = create(:product, name: 'T-shirts',
                               description: 'Good T-shirts',
                               price: 100)
    product.images.create!(attachment: create(:image).attachment)

    visit potepan_product_url(id: product.slug)

    expect(page).to have_title    "#{product.name} - Potepanec"
    within 'div.media-body' do
      expect(page).to     have_content('T-shirts')
      expect(page).to     have_content('100.00')
      expect(page).to     have_content('Good T-shirts')
      expect(page).to     have_content('ALL')
      expect(page).to     have_content('現在在庫切れ')
      expect(page).to_not have_content('一覧ページへ戻る')
    end
    img_path = product.images.first.attachment(:large)
    within 'div#carousel' do
      expect(page).to have_css("img[src*='#{img_path}']")
    end
    thumb_path = product.images.first.attachment(:product)
    within 'div#thumbcarousel' do
      expect(page).to have_css("img[src*='#{thumb_path}']")
    end
  end

  scenario '関連商品やバリエーションを持つ商品のページを操作する', js: true do
    setup_prodocts_with_image_and_taxon

    visit potepan_product_path(id: @product.slug)

    within 'div.media-body' do
      expect(page).to have_content('Pants-0')
      expect(page).to have_content('123.45')
      expect(page).to have_content('Size: S')
      expect(page).to have_content('残り 10 点')
      expect(page).to have_link(
        '一覧ページへ戻る', href: potepan_category_path(@product.taxon_ids[0])
      )
    end
    within 'div.productsContent' do
      expect(page).to_not have_content('Pants-0')
      expect(page).to_not have_content('123.45')
      expect(page).to_not have_link(href: potepan_product_path('pants-0'))
      expect(page).to have_content('Pants-1')
      expect(page).to have_content('200.00')
      expect(page).to have_link(
        href: potepan_product_path('pants-1')
      )
      expect(page.all('img').size).to         eq(4)
      expect(page.all('.productBox').size).to eq(4)
    end
    # クリック操作の代わりに直接リクエスト送信
    visit potepan_product_path(id: @product.slug, variant_id: @variant.id)

    within 'div.media-body' do
      expect(page).to have_content('123.45')
      expect(page).to have_content('残り 10 点')
    end
  end

  def setup_prodocts_with_image_and_taxon
    taxon = create(:taxon)
    5.times do |index|
      product = create(:product_with_option_types,
                       name: "Pants-#{index}",
                       slug: "pants-#{index}",
                       taxons: [taxon],
                       price: 100 * (index + 1))
      product.images.create!(attachment: create(:image).attachment)
    end
    @product = Spree::Product.find_by!(slug: 'pants-0')
    @variant = create(:variant,
                      product: @product,
                      price: 123.45)
    @variant.images.create!(attachment: create(:image).attachment)
    @variant.stock_items = [create(:stock_item)]
  end

  describe '商品検索ページを操作する' do
    include_context 'products_search_setup'

    def search_products(product_name)
      find('.searchBox').hover
      fill_in 'keyword', with: product_name
      find(:xpath, '//*[@id="basic-addon2"]').click
    end

    before do
      visit potepan_root_path
      search_products('Shirt')
    end

    scenario 'ページヘッダの検索窓から検索を実行する', js: true do
      expect(page).to have_title   '検索結果 - Potepanec'
      expect(page).to have_content '検索条件：Shirt'
      within '#show_products' do
        expect(page).to have_content 'Good-Shirt'
        expect(page).to have_content '900'
        expect(all('.productBox').size).to eq(3)
      end
    end

    scenario '検索窓のキーワードによって商品表示が動的に切り替わること', js: true do
      within '#show_products' do
        expect(page).to     have_content 'Good-Shirt'
        expect(page).to_not have_content 'Creepy-Bag'
        expect(all('.productBox').size).to eq(3)
      end

      search_products('Bag')
      expect(page).to have_content '検索条件：Bag'
      within '#show_products' do
        expect(page).to_not have_content 'Good-Shirt'
        expect(page).to     have_content 'Creepy-Bag'
        expect(all('.productBox').size).to eq(1)
      end
    end

    scenario '色から絞り込みを行う', js: true do
      click_link 'Red'
      within '#color-filter-panel' do
        expect(page).to have_content(/Red.+✔︎/)
      end
      within '#show_products' do
        expect(page).to     have_content 'Good-Shirt'
        expect(page).to_not have_content 'Bad-Shirt'
        expect(all('.productBox').size).to eq(2)
      end
    end

    scenario 'サイズから絞り込みを行う', js: true do
      click_link 'Size-S'
      within '#size-filter-panel' do
        expect(page).to have_content(/S.+✔︎/)
      end
      within '#show_products' do
        expect(page).to     have_content('Good-Shirt')
        expect(page).to_not have_content('Bad-Shirt')
        expect(all('.productBox').size).to eq(1)
      end
    end

    scenario '商品表示形式を変更する', js: true do
      click_link('List')
      within '#show_products' do
        expect(page).to     have_content('Good-Shirt')
        expect(page).to     have_content('High quality')
        expect(all('.media').size).to eq(3)
      end
    end

    scenario '商品をソートする', js: true do
      find('.sbSelector').click
      click_on('安い順')
      sleep(3)
      within 'div#show_products' do
        product_name = page.all('h5')
        expect(product_name[0].text).to eq('Bad-Shirt')
        expect(product_name[1].text).to eq('Normal-Shirt')
        expect(product_name[2].text).to eq('Good-Shirt')
      end
    end
  end

  describe 'ソートロジックのパターン別検証' do
    include_context 'products_search_setup'

    shared_examples '指定したソートタイプの通りに並ぶこと' do
      it '指定したソートタイプの通りに並ぶこと' do
        within 'div#show_products' do
          product_name = page.all('h5')
          expect(product_name[0].text).to eq('Bad-Shirt')
          expect(product_name[1].text).to eq('Normal-Shirt')
          expect(product_name[2].text).to eq('Good-Shirt')
        end
      end
    end

    context '新着順の場合' do
      let(:sort_type) { '0' }
      before do
        visit potepan_products_path(keyword: 'Shirt', sort_type: sort_type)
      end

      it_behaves_like '指定したソートタイプの通りに並ぶこと'
    end

    context '高い順の場合' do
      let(:sort_type) { '2' }
      before do
        bad_shirts.update(price:    900.00)
        normal_shirts.update(price: 500.00)
        good_shirts.update(price:   100.00)
        visit potepan_products_path(keyword: 'Shirt', sort_type: sort_type)
      end

      it_behaves_like '指定したソートタイプの通りに並ぶこと'
    end

    context '古い順の場合' do
      let(:sort_type) { '3' }
      before do
        bad_shirts.update(available_on:    4.days.ago)
        normal_shirts.update(available_on: 3.days.ago)
        good_shirts.update(available_on:   2.days.ago)
        visit potepan_products_path(keyword: 'Shirt', sort_type: sort_type)
      end

      it_behaves_like '指定したソートタイプの通りに並ぶこと'
    end
  end
end
