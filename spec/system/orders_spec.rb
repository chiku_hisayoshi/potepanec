require 'rails_helper'

RSpec.describe 'Orders', type: :system do
  include_context 'orders_spec_setup'

  def variation_in_cart(product:, variation:, quantity:)
    visit potepan_product_path(product)
    page.all('.sbSelector')[0].click
    page.all('a', text: variation)[1].click
    page.all('.sbSelector')[1].click
    click_on(quantity)
    find('.order').click
  end

  describe '初期状態でのカートページの操作' do
    before do
      visit potepan_root_path
      find('#cart-page-link').click
    end

    scenario 'レイアウトが正しいこと' do
      expect(page).to have_title('cart')
    end

    scenario 'なにも商品が登録されていないこと' do
      expect(page).to have_no_css('.line-item')
      expect(page).to have_content('カートに商品はありません')
    end
  end

  describe '商品個別ページから商品をカートに入れる操作' do
    context '１つのバリエーションのみカートに入れた場合' do
      before do
        variation_in_cart(product: good_shirts,
                          variation: 'Color: Red, Size: Size-S',
                          quantity: '2')
      end

      scenario 'カートページに商品が保存されていること', js: true do
        within '.line-item' do
          expect(page.all('img').size).to eq(1)
          expect(page).to have_content('Good-Shirt')
          expect(page).to have_content('Color: Red, Size: Size-S')
          expect(page).to have_content(/900.00/)
          xpath = '//*[@id="order_line_items_attributes_0_quantity"][@value=2]'
          expect(page).to have_xpath(xpath)
          expect(page).to have_content(/1,800.00/)
        end
      end

      scenario 'カートページ内の小計、消費税、総計が正しいこと', js: true do
        within '.totalAmountArea' do
          expect(page.find('.itemTotal').text).to  have_content(/1,800.00/)
          expect(page.find('.taxAmount').text).to  have_content(/90.00/)
          expect(page.find('.grandTotal').text).to have_content(/1,890.00/)
        end
      end
    end

    context '複数のバリエーションをカートに入れた場合' do
      before do
        variation_in_cart(product: good_shirts,
                          variation: 'Color: Red, Size: Size-S',
                          quantity: '2')
        variation_in_cart(product: bad_shirts,
                          variation: 'Color: Blue, Size: Size-M',
                          quantity: '3')
      end

      scenario 'カートページに商品が複数保存されていること', js: true do
        within '.table-responsive' do
          expect(page.all('img').size).to eq(2)
          expect(page).to have_content('Good-Shirt')
          expect(page).to have_content('Color: Red, Size: Size-S')
          expect(page).to have_content('Bad-Shirt')
          expect(page).to have_content('Color: Blue, Size: Size-M')
        end
      end

      scenario 'カートページ内の小計、消費税、総計が正しいこと', js: true do
        within '.totalAmountArea' do
          expect(page.find('.itemTotal').text).to  have_content(/2,100.00/)
          expect(page.find('.taxAmount').text).to  have_content(/105.00/)
          expect(page.find('.grandTotal').text).to have_content(/2,205.00/)
        end
      end
    end

    context '同一バリエーションを複数回カートに入れた場合' do
      before do
        variation_in_cart(product: good_shirts,
                          variation: 'Color: Red, Size: Size-S',
                          quantity: '2')
        variation_in_cart(product: good_shirts,
                          variation: 'Color: Red, Size: Size-S',
                          quantity: '2')
      end

      scenario 'カートページの商品個数が注文分の合計値になっていること', js: true do
        within '.line-item' do
          expect(page.all('img').size).to eq(1)
          expect(page).to have_content('Good-Shirt')
          expect(page).to have_content('Color: Red, Size: Size-S')
          expect(page).to have_content(/900.00/)
          xpath = '//*[@id="order_line_items_attributes_0_quantity"][@value=4]'
          expect(page).to have_xpath(xpath)
          expect(page).to have_content(/3,600.00/)
        end
      end
    end
  end

  describe 'カートから商品を削除する操作' do
    before do
      variation_in_cart(product: good_shirts,
                        variation: 'Color: Red, Size: Size-S',
                        quantity: '2')
      variation_in_cart(product: bad_shirts,
                        variation: 'Color: Blue, Size: Size-M',
                        quantity: '3')
    end

    scenario '×ボタンをクリックした商品がカートから削除されること', js: true do
      within '.table-responsive' do
        expect(page.all('img').size).to eq(2)
        expect(page).to have_content('Good-Shirt')
        expect(page).to have_content('Color: Red, Size: Size-S')
        expect(page).to have_content('Bad-Shirt')
        expect(page).to have_content('Color: Blue, Size: Size-M')
      end
      page.all('.close')[0].click
      within '.table-responsive' do
        expect(page.all('img').size).to eq(1)
        expect(page).to_not have_content('Good-Shirt')
        expect(page).to_not have_content('Color: Red, Size: Size-S')
        expect(page).to     have_content('Bad-Shirt')
        expect(page).to     have_content('Color: Blue, Size: Size-M')
      end
    end

    scenario 'カートの小計,消費税,総計が減算されること', js: true do
      within '.totalAmountArea' do
        expect(page.find('.itemTotal').text).to  have_content(/2,100.00/)
        expect(page.find('.taxAmount').text).to  have_content(/105.00/)
        expect(page.find('.grandTotal').text).to have_content(/2,205.00/)
      end
      page.all('.close')[0].click
      within '.totalAmountArea' do
        expect(page.find('.itemTotal').text).to  have_content(/300.00/)
        expect(page.find('.taxAmount').text).to  have_content(/15.00/)
        expect(page.find('.grandTotal').text).to have_content(/315.00/)
      end
    end
  end

  describe 'カートの内容を更新する操作' do
    before do
      variation_in_cart(product: good_shirts,
                        variation: 'Color: Red, Size: Size-S',
                        quantity: '2')
      variation_in_cart(product: bad_shirts,
                        variation: 'Color: Blue, Size: Size-M',
                        quantity: '2')
      first('input[type="number"]').set(99)
      find('#update-btn').click
    end

    scenario 'カートに入っている商品の個数が更新されること', js: true do
      xpath = '//*[@id="order_line_items_attributes_0_quantity"][@value=99]'
      expect(page).to have_xpath(xpath)
      expect(page).to have_content(/89,100/)
    end

    scenario '小計、個数、総計が更新されること', js: true do
      within '.totalAmountArea' do
        expect(page.find('.itemTotal').text).to  have_content(/89,300.00/)
        expect(page.find('.taxAmount').text).to  have_content(/4,465.00/)
        expect(page.find('.grandTotal').text).to have_content(/93,765.00/)
      end
    end

    scenario 'フラッシュメッセージが表示されること', js: true do
      css = 'div.flash-success'
      expect(page).to have_css(css)
      within css do
        expect(page).to have_content(/更新しました/)
      end
    end
  end
end
