require 'rails_helper'

RSpec.describe 'Categories', type: :system do
  include_context 'categories_spec_setup'

  def filtered_color_link(color)
    params = { id: shirts.id, display_style: 'grid' }
    params.merge!(filter_color: color.id) if color.present?
    potepan_category_path(params)
  end

  def filtered_size_link(size)
    params = { id: shirts.id, display_style: 'grid' }
    params.merge!(filter_size: size.id) if size.present?
    potepan_category_path(params)
  end

  def retry_click_link_and_wait_for_contents(link, contents)
    Timeout.timeout(Capybara.default_max_wait_time) do
      loop do
        click_link(link)
        break if has_content?(contents)
      end
    end
  end

  describe 'カテゴリページの操作' do
    before { visit potepan_category_url(id: shirts.id) }

    def expect_to_products_show_correct
      expect(page).to have_css('img')
      expect(page).to have_link(href: potepan_product_path(good_shirts))
      expect(page).to have_content('Good-Shirt')
      expect(page).to have_content('$900.00')
      expect(page).to have_css('img')
      expect(page).to have_link(href: potepan_product_path(bad_shirts))
      expect(page).to have_content('Bad-Shirt')
      expect(page).to have_content('$100.00')
      expect(page).to have_content('バリエーションあり')
    end

    scenario 'カテゴリページに通常アクセスする' do
      expect(page).to have_title 'カテゴリ - Shirts - Potepanec'
      node = first(:css, 'div.panel-body')
      within node do
        expect(page).to have_content('Category')
        expect(page).to have_content('Shirts ( 3 )')
        expect(page).to have_content('Tote ( 1 )')
      end
      within 'div#show_products' do
        expect(page).to_not have_css('div.productListSingle')
        expect_to_products_show_correct
        expect(page.all('h6').size).to eq(3)
      end
      click_link('Tote ( 1 )')
      within 'div#show_products' do
        expect(page).to_not have_css('div.productListSingle')
        expect(page).to have_content('Good-Tote')
        expect(page).to_not have_content('バリエーションあり')
      end
    end

    scenario '通常アクセス後、LIST表示に切り替える' do
      within 'div.btn-group' do
        click_link('List')
      end
      link = potepan_category_path(id: shirts.id, display_style: 'list')
      expect(page).to have_link(href: link)
      within 'div#show_products' do
        expect(page).to have_css('div.productListSingle')
        expect(page).to have_content('High quality')
        expect(page).to have_content('Low quality')
        expect_to_products_show_correct
      end
    end

    scenario '色フィルターの操作' do
      expect_to_products_show_correct
      within '#color-filter-panel' do
        expect(page).to have_link('Red',  href: filtered_color_link(color_red))
        expect(page).to have_link('Blue', href: filtered_color_link(color_blue))
      end
      click_link 'Red'
      within '#color-filter-panel' do
        expect(page).to have_content(/Red.+✔︎/)
        expect(page).to have_link(href: filtered_color_link(nil))
        expect(page).to have_link('Blue', href: filtered_color_link(color_blue))
      end
      within '#show_products' do
        expect(page).to     have_css('img')
        expect(page).to     have_link(href: potepan_product_path(good_shirts))
        expect(page).to     have_content('Good-Shirt')
        expect(page).to     have_content('$900.00')
        expect(page).to     have_content('条件に合致するバリエーションあり')
        expect(page).to_not have_content('Bad-Shirt')
      end
    end

    scenario 'サイズフィルターの操作' do
      expect_to_products_show_correct
      within '#size-filter-panel' do
        expect(page).to have_link('Size-S', href: filtered_size_link(size_s))
        expect(page).to have_link('Size-M', href: filtered_size_link(size_m))
      end
      click_link 'Size-S'
      within '#size-filter-panel' do
        expect(page).to have_content(/Size-S.+✔︎/)
        expect(page).to have_link(href: filtered_size_link(nil))
        expect(page).to have_link('Size-M', href: filtered_size_link(size_m))
      end
      within '#show_products' do
        expect(page).to     have_css('img')
        expect(page).to     have_link(href: potepan_product_path(good_shirts))
        expect(page).to     have_content('Good-Shirt')
        expect(page).to     have_content('$900.00')
        expect(page).to     have_content('条件に合致するバリエーションあり')
        expect(page).to_not have_content('Bad-Shirt')
      end
    end
  end

  describe '色フィルタの詳細確認' do
    before { visit potepan_category_url(id: shirts.id) }

    context '別の色に切り替えたとき' do
      scenario 'フィルタ操作により、商品が切り替わる' do
        click_link 'Red'
        within '#show_products' do
          expect(page).to     have_content('Good-Shirt')
          expect(page).to_not have_content('Bad-Shirt')
          expect(page.all('.productBox').size).to eq(2)
        end
        click_link 'Blue'
        within '#show_products' do
          expect(page).to_not have_content('Good-Shirt')
          expect(page).to     have_content('Bad-Shirt')
          expect(page.all('.productBox').size).to eq(1)
        end
      end

      scenario 'フィルタ操作により、選択中状態が切り替わる' do
        click_link 'Red'
        within '#color-filter-panel' do
          expect(page).to have_content(/Red.+✔︎/)
          expect(page).to have_link(href: filtered_color_link(nil))
          expect(page).to have_link('Blue',
                                    href: filtered_color_link(color_blue))
        end
        click_link 'Blue'
        within '#color-filter-panel' do
          expect(page).to have_link('Red', href: filtered_color_link(color_red))
          expect(page).to have_content(/Blue.+✔︎/)
          expect(page).to have_link('Blue', href: filtered_color_link(nil))
        end
      end
    end

    context '表示形式を切り替えたとき' do
      before do
        click_link('Red')
        click_link('List')
      end

      scenario '色フィルタが解除されないこと' do
        within '#show_products' do
          expect(page).to     have_content('Good-Shirt')
          expect(page).to_not have_content('Bad-Shirt')
          expect(page.all('.media').size).to eq(2)
        end
      end

      scenario '色フィルタ選択中状態が解除されないこと' do
        within '#color-filter-panel' do
          expect(page).to have_content(/Red.+✔︎/)
          expect(page).to have_link(
            href: potepan_category_path(id: shirts.id, display_style: 'list')
          )
        end
      end
    end

    context 'ソートしたとき' do
      scenario '色フィルタが解除されないこと', js: true do
        retry_click_link_and_wait_for_contents('Red', /Red.+✔︎/)
        within '#color-filter-panel' do
          expect(page).to have_content(/Red.+✔︎/)
          expect(page).to have_link(href: filtered_color_link(nil))
        end
        find('.sbSelector').click
        click_on('安い順')
        within '#show_products' do
          expect(page).to     have_content('Good-Shirt')
          expect(page).to_not have_content('Bad-Shirt')
          expect(page.all('.productBox').size).to eq(2)
        end
      end
    end
  end

  describe 'サイズフィルタの詳細確認' do
    before { visit potepan_category_url(id: shirts.id) }

    context '別のサイズに切り替えたとき' do
      scenario 'フィルタ操作により、商品が切り替わる' do
        click_link 'Size-S'
        within '#show_products' do
          expect(page).to     have_content('Good-Shirt')
          expect(page).to_not have_content('Bad-Shirt')
          expect(page.all('.productBox').size).to eq(1)
        end
        click_link 'Size-M'
        within '#show_products' do
          expect(page).to_not have_content('Good-Shirt')
          expect(page).to     have_content('Bad-Shirt')
          expect(page.all('.productBox').size).to eq(2)
        end
      end

      scenario 'フィルタ操作により、選択中状態が切り替わる' do
        click_link 'Size-S'
        within '#size-filter-panel' do
          expect(page).to have_content(/Size-S.+✔︎/)
          expect(page).to have_link(href: filtered_size_link(nil))
          expect(page).to have_link('Size-M', href: filtered_size_link(size_m))
        end
        click_link 'Size-M'
        within '#size-filter-panel' do
          expect(page).to have_content(/Size-M.+✔︎/)
          expect(page).to have_link(href: filtered_size_link(nil))
          expect(page).to have_link('Size-S',
                                    href: filtered_size_link(size_s))
        end
      end
    end

    context '表示形式を切り替えたとき' do
      before do
        click_link('Size-S')
        click_link('List')
      end

      scenario 'サイズフィルタが解除されないこと' do
        within '#show_products' do
          expect(page).to     have_content('Good-Shirt')
          expect(page).to_not have_content('Bad-Shirt')
          expect(page.all('.media').size).to eq(1)
        end
        within '#size-filter-panel' do
          expect(page).to have_content(/Size-S.+✔︎/)
          expect(page).to have_link(
            href: potepan_category_path(id: shirts.id, display_style: 'list')
          )
        end
      end
    end

    context 'ソートしたとき' do
      scenario 'サイズフィルタが解除されないこと', js: true do
        retry_click_link_and_wait_for_contents('Size-M', /Size-M.+✔︎/)
        within '#size-filter-panel' do
          expect(page).to have_content(/Size-M.+✔︎/)
          expect(page).to have_link(href: filtered_size_link(nil))
        end
        find('.sbSelector').click
        click_on('安い順')
        within '#show_products' do
          expect(page).to_not have_content('Good-Shirt')
          expect(page).to     have_content('Bad-Shirt')
          expect(page.all('.productBox').size).to eq(2)
        end
      end
    end
  end

  describe 'フィルタの複合条件時の詳細確認' do
    before { visit potepan_category_url(id: shirts.id) }

    context '別の色,サイズに切り替えたとき' do
      scenario 'フィルタ操作により、商品が切り替わる' do
        click_link 'Red'
        click_link 'Size-S'
        within '#show_products' do
          expect(page).to     have_content('Good-Shirt')
          expect(page).to_not have_content('Bad-Shirt')
          expect(page.all('.productBox').size).to eq(1)
        end
        click_link 'Size-M'
        within '#show_products' do
          expect(page).to_not have_content('Good-Shirt')
          expect(page).to     have_content('Normal-Shirt')
          expect(page.all('.productBox').size).to eq(1)
        end
        click_link 'Blue'
        within '#show_products' do
          expect(page).to_not have_content('Normal-Shirt')
          expect(page).to     have_content('Bad-Shirt')
          expect(page.all('.productBox').size).to eq(1)
        end
      end

      scenario 'フィルタ操作により、選択中状態が切り替わる' do
        click_link 'Red'
        click_link 'Size-S'
        within '#color-filter-panel' do
          expect(page).to have_content(/Red.+✔︎/)
          expect(page).to have_content('Blue')
        end
        within '#size-filter-panel' do
          expect(page).to have_content(/Size-S.+✔︎/)
          expect(page).to have_content('Size-M')
        end
        click_link 'Size-M'
        within '#color-filter-panel' do
          expect(page).to have_content(/Red.+✔︎/)
          expect(page).to have_content('Blue')
        end
        within '#size-filter-panel' do
          expect(page).to have_content(/Size-M.+✔︎/)
          expect(page).to have_content('Size-S')
        end
        click_link 'Blue'
        within '#color-filter-panel' do
          expect(page).to have_content(/Blue.+✔︎/)
          expect(page).to have_content('Red')
        end
        within '#size-filter-panel' do
          expect(page).to have_content(/Size-M.+✔︎/)
          expect(page).to have_content('Size-S')
        end
      end
    end

    context '表示形式を切り替えたとき' do
      before do
        click_link('Red')
        click_link('Size-S')
        click_link('List')
      end

      scenario '色、サイズフィルタが解除されないこと' do
        within '#show_products' do
          expect(page).to     have_content('Good-Shirt')
          expect(page).to_not have_content('Bad-Shirt')
          expect(page.all('.media').size).to eq(1)
        end
      end

      scenario '色、サイズフィルタの選択中状態が解除されないこと' do
        within '#color-filter-panel' do
          expect(page).to have_content(/Red.+✔︎/)
        end
        within '#size-filter-panel' do
          expect(page).to have_content(/Size-S.+✔︎/)
        end
      end
    end

    context 'ソートしたとき' do
      scenario '色、サイズフィルタが解除されないこと', js: true do
        retry_click_link_and_wait_for_contents('Blue', /Blue.+✔︎/)
        within '#color-filter-panel' do
          expect(page).to have_content(/Blue.+✔︎/)
        end
        retry_click_link_and_wait_for_contents('Size-M', /Size-M.+✔︎/)
        within '#size-filter-panel' do
          expect(page).to have_content(/Size-M.+✔︎/)
        end
        find('.sbSelector').click
        click_on('安い順')
        within '#show_products' do
          expect(page).to_not have_content('Good-Shirt')
          expect(page).to     have_content('Bad-Shirt')
          expect(page.all('.productBox').size).to eq(1)
        end
      end
    end
  end

  describe 'ソートの確認' do
    before { visit potepan_category_url(id: shirts.id) }

    context 'デフォルト(ソートなし)の場合', js: true do
      scenario '新着順に並べる' do
        expect_sort_to_be_success
      end
    end

    context 'ソートの選択を変更した場合' do
      before { find('.sbSelector').click }

      scenario '新着順に並べる', js: true do
        page.all('a', text: '新着順')[1].click
        expect_sort_to_be_success
      end

      scenario '安い順に並べる', js: true do
        click_on('安い順')
        expect_sort_to_be_success
      end

      scenario '高い順に並べる', js: true do
        bad_shirts.update(price:    900.00)
        normal_shirts.update(price: 500.00)
        good_shirts.update(price:   100.00)
        click_on('高い順')
        expect_sort_to_be_success
      end

      scenario '古い順に並べる', js: true do
        bad_shirts.update(available_on:    4.days.ago)
        normal_shirts.update(available_on: 3.days.ago)
        good_shirts.update(available_on:   2.days.ago)
        click_on('古い順')
        expect_sort_to_be_success
      end
    end

    def expect_sort_to_be_success
      Timeout.timeout(Capybara.default_max_wait_time) do
        loop until page.evaluate_script('jQuery.active').zero?
      end
      within 'div#show_products' do
        product_name = page.all('h5')
        expect(product_name[0].text).to eq 'Bad-Shirt'
        expect(product_name[1].text).to eq 'Normal-Shirt'
        expect(product_name[2].text).to eq 'Good-Shirt'
      end
    end
  end
end
