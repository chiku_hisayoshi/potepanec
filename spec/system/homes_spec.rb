require 'rails_helper'

RSpec.describe 'Homes', type: :system do
  describe 'トップページの操作' do
    include_context 'new_arrival_spec_setup'
    before { visit potepan_root_url }

    scenario 'タイトル, ヘッダリンクが適切に表示されていることを確認する' do
      expect(page).to have_title 'Potepanec'
      within '.header' do
        expect(page.all("a[href='#{potepan_root_path}']").size).to eq(2)
      end
    end

    scenario '新着商品が表示されていることを確認' do
      within '.featuredProductsSlider' do
        expect(page).to have_content('NewArraival')
        expect(page).to have_content('100.00')
        expect(page).to have_selector('img')
        expect(page).to have_link(href: potepan_product_path(new_arrival))
        expect(page.all('.slide').size).to eq(2)
      end
    end
    scenario '新着順に表示されていること' do
      within '.featuredProductsSlider' do
        product_name = page.all('h5')
        expect(product_name[0].text).to eq('RecentArraival')
        expect(product_name[1].text).to eq('NewArraival')
      end
    end
  end

  describe '人気カテゴリーの操作' do
    include_context 'popular_categories_setup'

    before { visit potepan_root_path }

    scenario '人気カテゴリーが表示されていること' do
      within '.featuredCollection' do
        expect(page.all('img').size).to eq(2)
        expect(page).to have_content('Shirts')
        expect(page).to have_content('Tote')
        expect(page).to_not have_content('Short Socks')
      end
    end

    scenario '表示している画像をクリックすると、カテゴリーページに飛ぶこと' do
      first('.thumbnail').click
      click_on('View Shirts')
      within '.page-title' do
        expect(page).to have_content('Shirts')
      end
      within '#show_products' do
        expect(page).to have_content('Good-Shirt')
      end
    end
  end
end
