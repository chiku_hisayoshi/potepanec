Capybara.javascript_driver = :selenium_chrome_headless
Capybara.register_driver :selenium_chrome_headless do |app|
  browser_options = ::Selenium::WebDriver::Chrome::Options.new
  browser_options.args << '--headless'
  browser_options.args << '--no-sandbox'
  browser_options.args << '--disable-gpu'
  browser_options.args << '--blink-settings=imagesEnabled=false'
  browser_options.args << '--window-size=1920,1080'
  Capybara::Selenium::Driver.new(app, browser: :chrome,
                                      options: browser_options)
end
Capybara.ignore_hidden_elements = false
Capybara.default_max_wait_time  = 15
