RSpec.shared_context 'products_spec_variation_setup' do
  let(:product_with_option_types) do
    create(:product_with_option_types, name: 'product_with_option_types')
  end
  let!(:variant) do
    create(:variant, product: product_with_option_types, price: 999.99)
  end
end
