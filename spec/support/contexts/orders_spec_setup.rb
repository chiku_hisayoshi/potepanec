RSpec.shared_context 'orders_spec_setup' do
  # 消費税
  let!(:us) { create :country, iso: 'US' }
  let!(:us_zone) { create :zone, countries: [us] }
  let!(:default_store) do
    create(:store, name: 'My Store',
                   cart_tax_country_iso: us.iso)
  end
  let!(:default_tax_category) { create :tax_category, name: 'Default' }
  let!(:us_tax) do
    create(:tax_rate, name: 'US TAX',
                      included_in_price: false,
                      amount: 0.05,
                      tax_categories: [default_tax_category],
                      zone: us_zone)
  end

  # 商品オプション
  let!(:option_type_color) do
    create(:option_type,  name: 'shirt-color',
                          presentation: 'Color')
  end
  let!(:option_type_size) do
    create(:option_type,  name: 'shirt-size',
                          presentation: 'Size')
  end
  let!(:color_red) do
    create(:option_value, name: 'red',
                          presentation: 'Red',
                          option_type: option_type_color)
  end
  let!(:color_blue) do
    create(:option_value, name: 'blue',
                          presentation: 'Blue',
                          option_type: option_type_color)
  end
  let!(:size_s) do
    create(:option_value, name: 'size-s',
                          presentation: 'Size-S',
                          option_type: option_type_size)
  end
  let!(:size_m) do
    create(:option_value, name: 'size-m',
                          presentation: 'Size-M',
                          option_type: option_type_size)
  end

  # 商品
  let!(:good_shirts) do
    create(:product,  name: 'Good-Shirt',
                      description: 'High quality',
                      price: 900,
                      available_on: 6.days.ago,
                      option_types: [option_type_color, option_type_size],
                      tax_category: default_tax_category)
  end

  let!(:bad_shirts) do
    create(:product,  name: 'Bad-Shirt',
                      description: 'Low quality',
                      price: 100,
                      available_on: 2.days.ago,
                      option_types: [option_type_color, option_type_size],
                      tax_category: default_tax_category)
  end

  # 商品バリエーション
  let!(:good_shirts_red_s) do
    create(:variant,  product: good_shirts,
                      option_values: [color_red, size_s],
                      price: good_shirts.price,
                      tax_category: default_tax_category)
  end
  let!(:good_shirts_red_m) do
    create(:variant,  product: good_shirts,
                      option_values: [color_red, size_m],
                      price: good_shirts.price,
                      tax_category: default_tax_category)
  end
  let!(:good_shirts_blue_s) do
    create(:variant,  product: good_shirts,
                      option_values: [color_blue, size_s],
                      price: good_shirts.price,
                      tax_category: default_tax_category)
  end
  let!(:bad_shirts_blue_m) do
    create(:variant,  product: bad_shirts,
                      option_values: [color_blue, size_m],
                      price: bad_shirts.price,
                      tax_category: default_tax_category)
  end

  # ラインアイテム(カートに入っている商品)
  let!(:line_item_red_s) do
    create(:line_item, variant: good_shirts_red_s,
                       quantity: 1,
                       price: good_shirts_red_s.price,
                       tax_category: default_tax_category)
  end
  let!(:line_item_blue_s) do
    create(:line_item, variant: good_shirts_blue_s,
                       quantity: 1,
                       price: good_shirts_red_s.price,
                       tax_category: default_tax_category)
  end
  let!(:line_item_red_m) do
    create(:line_item, variant: good_shirts_red_m,
                       quantity: 1,
                       price: good_shirts_red_s.price,
                       tax_category: default_tax_category)
  end

  # 注文
  let!(:order_in_cart) do
    create(:order, line_items: [line_item_red_s, line_item_blue_s],
                   item_total: (line_item_red_s.price + line_item_blue_s.price),
                   total: (line_item_red_s.price + line_item_blue_s.price),
                   item_count: 2,
                   guest_token: 'guest_token')
  end
  let!(:other_order_in_cart) do
    create(:order, line_items: [line_item_red_m],
                   item_total: line_item_red_m.price,
                   total: line_item_red_m.price,
                   item_count: 1,
                   guest_token: 'other_token')
  end
end
