RSpec.shared_context 'popular_categories_setup' do
  let!(:taxonomy) { create(:taxonomy, name: 'Category') }
  let!(:clothing) { taxonomy.root.children.create(name: 'Clothing') }
  let!(:shirts) { clothing.children.create(name: 'Shirts', taxonomy: taxonomy) }
  let!(:good_shirts) do
    create(:product,  name: 'Good-Shirt',
                      taxons: [shirts])
  end

  let!(:bag)  { taxonomy.root.children.create(name: 'Bag') }
  let!(:tote) { bag.children.create(name: 'Tote', taxonomy: taxonomy) }
  let!(:good_tote) do
    create(:product,  name: 'Good-Tote',
                      taxons: [tote])
  end

  let!(:socks) { taxonomy.root.children.create(name: 'Socks') }
  let!(:short_socks) do
    socks.children.create(name: 'Short Socks', taxonomy: taxonomy)
  end
end
