RSpec.shared_context 'new_arrival_spec_setup' do
  let!(:recent_arrival) do
    create(:product, name: 'RecentArraival',
                     price: 50.00,
                     available_on: 2.days.ago)
  end
  let!(:new_arrival) do
    create(:product, name: 'NewArraival',
                     price: 100.00,
                     available_on: 6.days.ago)
  end
  let!(:old_product) do
    create(:product, name: 'OldProduct',
                     price: 200.00,
                     available_on: 8.days.ago)
  end
end
RSpec.shared_context 'new_arrival_spec_setup_only_old_product' do
  let!(:old_product) do
    create(:product, name: 'OldProduct',
                     available_on: 8.days.ago)
  end
end
