require 'rails_helper'

RSpec.describe 'Homes', type: :request do
  describe '新着商品の検証' do
    context '新着商品が存在する場合' do
      include_context 'new_arrival_spec_setup'
      before { get potepan_root_path }

      it '新着商品のみを表示する' do
        expect(response.body).to     include('RecentArraival')
        expect(response.body).to     include('NewArraival')
        expect(response.body).to_not include('OldProduct')
      end
    end

    context '新着商品が存在しない場合' do
      include_context 'new_arrival_spec_setup_only_old_product'
      before { get potepan_root_path }

      it '新着商品がない旨のメッセージを表示する' do
        expect(response.body).to_not include('OldProduct')
        expect(response.body).to     include('新着商品はありません')
      end
    end
  end

  describe '人気カテゴリーの検証' do
    include_context 'popular_categories_setup'
    before { get potepan_root_path }

    it '人気カテゴリーにtaxon名称を表示する' do
      expect(response.body).to include('Shirts')
      expect(response.body).to include('Tote')
    end

    it '商品を持たないtaxonは表示しないこと' do
      expect(response.body).to_not include('Short Socks')
    end

    it 'childrenを持つtaxonは表示しないこと' do
      expect(response.body).to_not include('Bag')
      expect(response.body).to_not include('Clothing')
    end

    it 'カテゴリページへのリンクを生成すること' do
      expect(response.body).to include(potepan_category_path(shirts.id))
      expect(response.body).to include(potepan_category_path(tote.id))
    end
  end
end
