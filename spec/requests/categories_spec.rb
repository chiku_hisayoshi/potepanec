require 'rails_helper'

RSpec.describe 'Categories', type: :request do
  include_context 'categories_spec_setup'

  describe 'カテゴリページの検証' do
    before { get potepan_category_path(id: shirts.id) }

    it 'taxonに基づく商品の一覧を表示すること' do
      expect(response).to have_http_status '200'
      expect(response.body).to include('Good-Shirt')
      expect(response.body).to include('Bad-Shirt')
      expect(response.body).to include('Normal-Shirt')
      get potepan_category_path(id: tote.id)
      expect(response.body).to include('Good-Tote')
    end

    it 'taxonomyに基づくtaxonの一覧を表示すること' do
      expect(response).to have_http_status '200'
      expect(response.body).to include('Category')
      expect(response.body).to include('Shirts')
      expect(response.body).to include('Tote')
    end
  end

  describe '色フィルタの検証' do
    context '商品のオプションとして色がある場合' do
      before { get potepan_category_path(id: shirts.id) }

      it '色フィルタを表示する' do
        expect(response.body).to     include('Red')
        expect(response.body).to     include('Blue')
        expect(response.body).to_not include('Yellow')
      end

      it 'クリックすると、その色のオプションを持つ商品のみ表示されること' do
        get potepan_category_path(id: shirts.id, filter_color: color_red.id)
        expect(response.body).to     include('Good-Shirt')
        expect(response.body).to     include('Normal-Shirt')
        expect(response.body).to_not include('Bad-Shirt')
        get potepan_category_path(id: shirts.id, filter_color: color_blue.id)
        expect(response.body).to_not include('Good-Shirt')
        expect(response.body).to_not include('Normal-Shirt')
        expect(response.body).to     include('Bad-Shirt')
      end
    end

    context '商品のオプションとして色がない場合' do
      before { get potepan_category_path(id: tote.id) }

      it 'オプションがない旨のメッセージを表示する' do
        expect(response.body).to_not include('Red')
        expect(response.body).to     include('色のオプションはありません')
      end
    end
  end

  describe 'サイズフィルタの検証' do
    context '商品のオプションとしてサイズがある場合' do
      before { get potepan_category_path(id: shirts.id) }

      it 'サイズフィルタを表示する' do
        expect(response.body).to     include('Size-S')
        expect(response.body).to     include('Size-M')
        expect(response.body).to_not include('Size-L')
      end

      it 'クリックすると、そのサイズのオプションを持つ商品のみ表示されること' do
        get potepan_category_path(id: shirts.id, filter_size: size_s.id)
        expect(response.body).to     include('Good-Shirt')
        expect(response.body).to_not include('Normal-Shirt')
        expect(response.body).to_not include('Bad-Shirt')
        get potepan_category_path(id: shirts.id, filter_size: size_m.id)
        expect(response.body).to_not include('Good-Shirt')
        expect(response.body).to     include('Normal-Shirt')
        expect(response.body).to     include('Bad-Shirt')
      end
    end

    context '商品のオプションとしてサイズがない場合' do
      before { get potepan_category_path(id: tote.id) }

      it 'オプションがない旨のメッセージを表示する' do
        expect(response.body).to_not include('Size-S')
        expect(response.body).to     include('サイズのオプションはありません')
      end
    end
  end

  describe 'フィルタの複合条件の検証' do
    context '複合条件に完全に合致する商品が存在する場合' do
      before do
        get potepan_category_path(id: shirts.id, filter_color: color_red.id,
                                  filter_size: size_s.id)
      end

      it 'その商品のみ表示する' do
        expect(response.body).to     include('Good-Shirt')
        expect(response.body).to_not include('Normal-Shirt')
        expect(response.body).to_not include('Bad-Shirt')
      end
    end

    context '複合条件に一部合致する商品がある場合' do
      before do
        get potepan_category_path(id: shirts.id, filter_color: color_blue.id,
                                  filter_size: size_s.id)
      end

      it '商品を表示せず、条件に合う商品がない旨のメッセージを表示すること' do
        expect_to_be_notified_no_items
      end
    end

    context '複合条件に合致する商品がない場合' do
      before do
        get potepan_category_path(id: shirts.id, filter_color: color_yellow.id,
                                  filter_size: size_l.id)
      end

      it '商品を表示せず、条件に合う商品がない旨のメッセージを表示すること' do
        expect_to_be_notified_no_items
      end
    end

    def expect_to_be_notified_no_items
      expect(response.body).to     include('条件に合致する商品はありません')
      expect(response.body).to_not include('Good-Shirt')
      expect(response.body).to_not include('Normal-Shirt')
      expect(response.body).to_not include('Bad-Shirt')
    end
  end

  describe '表示形式の検証(grid/list/other)' do
    context 'クエリパラメータがgridの場合' do
      it 'show.js.erbファイルがレンダーされること' do
        expect_to_be_success('grid')
      end
    end

    context 'クエリパラメータがlistの場合' do
      it 'show.js.erbファイルがレンダーされること' do
        expect_to_be_success('list')
      end
    end

    context 'クエリパラメータが上記以外の場合' do
      it 'show.js.erbファイルがレンダーされること' do
        expect_to_be_success('foobar')
      end
    end

    def expect_to_be_success(display_style)
      get potepan_category_path(id: shirts.id)
      params = { display_style: display_style }
      get potepan_category_path(id: shirts.id), xhr: true, params: params
      expect(response).to have_http_status '200'
      expect(response.content_type).to eq 'text/javascript'
    end
  end
end
