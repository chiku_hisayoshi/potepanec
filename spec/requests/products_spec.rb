require 'rails_helper'

RSpec.describe 'Products', type: :request do
  describe '商品詳細ページの検証' do
    let(:product) { create(:product, name: 'hogehoge', price: 888.88) }

    describe '商品詳細ページを適切に表示する(#show)' do
      it '商品名がページ中に含まれていること' do
        get potepan_product_path id: product.slug
        expect(response.body).to include('hogehoge')
      end
      context '商品にバリエーションがある場合' do
        include_context 'products_spec_variation_setup'
        it '最初のバリエーションの値段がページ中に表示される' do
          get potepan_product_path id: product_with_option_types.slug
          expect(response.body).to include('999.99')
        end
      end

      context '商品にバリエーションがない場合' do
        it '商品の値段(マスター)の値段がページ中に表示される' do
          get potepan_product_path id: product.slug
          expect(response.body).to include('888.88')
        end
      end
    end

    describe '商品のバリエーションを切り替える(#show with Ajax)' do
      include_context 'products_spec_variation_setup'
      context 'バリエーションIDが正常に送信された場合' do
        it 'show.js.erbファイルがレンダーされること' do
          other_variant = create(:variant,
                                 product: product_with_option_types,
                                 price: 777.77)

          get potepan_product_path id: product_with_option_types.slug
          get potepan_product_path, xhr: true,
                                    params: { variant_id: other_variant.id }
          expect(response).to be_successful
          expect(response.content_type).to eq 'text/javascript'
          expect(response.body).to include('777.77')
        end
      end
      context 'バリエーションIDが不正である場合' do
        it '最初のバリエーションが表示される' do
          get potepan_product_path id: product_with_option_types.slug
          get potepan_product_path, xhr: true,
                                    params: { variant_id: '' }
          expect(response.body).to include('999.99')
        end
      end
    end

    def setup_products_with_taxons(hash)
      hash.stringify_keys.each do |taxon_name, products_num|
        skip_taxon_process = taxon_name.eql?('notaxon')
        taxon = create(:taxon, name: taxon_name) unless skip_taxon_process
        products_num.times do |index|
          product = create(:product, name: "#{taxon_name}-#{index}")
          product.taxons << taxon unless skip_taxon_process
        end
      end
    end

    describe '関連商品をページに表示する' do
      it '関連しない商品は表示しないこと' do
        setup_products_with_taxons(shirt: 2, pants: 2)
        get potepan_product_path id: 'shirt-0'
        expect(response.body).to     include('shirt-1')
        expect(response.body).to_not include('pants-1')
        get potepan_product_path id: 'pants-0'
        expect(response.body).to_not include('shirt-1')
        expect(response.body).to     include('pants-1')
      end

      context '関連商品が4つ以上ある場合' do
        it '4つの関連商品が表示されること' do
          setup_products_with_taxons(shirt: 5)
          get potepan_product_path id: 'shirt-0'
          expect(response.body).to include('shirt-1')
          expect(response.body).to include('shirt-2')
          expect(response.body).to include('shirt-3')
          expect(response.body).to include('shirt-4')
        end
      end

      context '関連商品が4つ未満である場合' do
        it 'その数の関連商品が表示されること' do
          setup_products_with_taxons(shirt: 3)
          get potepan_product_path id: 'shirt-0'
          expect(response.body).to include('shirt-1')
          expect(response.body).to include('shirt-2')
        end
      end

      context '関連商品が1つもない場合(taxonは存在する)' do
        it '関連商品がない旨のメッセージを表示すること' do
          setup_products_with_taxons(shirt: 1, pants: 1)
          get potepan_product_path id: 'pants-0'
          expect(response.body).to     include('この商品に関連する商品はありません')
          expect(response.body).to_not include('shirt-0')
        end
      end

      context '商品に対するtaxonが存在しない場合' do
        it '関連商品がない旨のメッセージを表示すること' do
          setup_products_with_taxons(shirt: 1, notaxon: 1)
          get potepan_product_path id: 'notaxon-0'
          expect(response.body).to     include('この商品に関連する商品はありません')
          expect(response.body).to_not include('shirt-0')
        end
      end
    end
  end

  describe '商品検索の検証' do
    include_context 'products_search_setup'
    before { get potepan_products_path, params: { keyword: keyword } }

    shared_examples 'Shirtのみ表示されていること' do
      it 'Shirtのみ表示されていること' do
        expect(response.body).to     include('Good-Shirt')
        expect(response.body).to     include('Normal-Shirt')
        expect(response.body).to     include('Bad-Shirt')
        expect(response.body).to_not include('Creepy-Bag')
      end
    end

    shared_examples '前方一致する商品のみ表示すること' do
      it '前方一致する商品のみ表示すること' do
        expect(response.body).to     include('Good-Shirt')
        expect(response.body).to_not include('Normal-Shirt')
        expect(response.body).to_not include('Bad-Shirt')
        expect(response.body).to_not include('Creepy-Bag')
      end
    end

    shared_examples '後方一致した商品のみ表示すること' do
      it '後方一致した商品のみ表示すること' do
        expect(response.body).to_not include('Good-Shirt')
        expect(response.body).to_not include('Normal-Shirt')
        expect(response.body).to_not include('Bad-Shirt')
        expect(response.body).to     include('Creepy-Bag')
      end
    end

    shared_examples '一致する商品がない旨のメッセージを表示すること' do
      it '一致する商品がない旨のメッセージを表示すること' do
        expect(response.body).to include('条件に合致する商品はありません')
      end
    end

    context '商品名の検索の場合' do
      context 'メタ文字なしの通常検索のとき(keyword)' do
        context 'キーワードに合致する商品があるとき' do
          let(:keyword) { 'Shirt' }
          it_behaves_like 'Shirtのみ表示されていること'
        end

        context 'キーワードに合致する商品がないとき' do
          let(:keyword) { 'Tote' }
          it_behaves_like '一致する商品がない旨のメッセージを表示すること'
        end
      end

      context 'メタ文字ありの検索のとき' do
        context '前方一致検索のとき(keyword%)' do
          context 'キーワードに合致する商品があるとき' do
            let(:keyword) { 'Good%' }
            it_behaves_like '前方一致する商品のみ表示すること'
          end

          context 'キーワードに合致する商品がないとき' do
            let(:keyword) { 'Tote%' }
            it_behaves_like '一致する商品がない旨のメッセージを表示すること'
          end
        end

        context '後方一致検索のとき(%keyword)' do
          context 'キーワードに合致する商品があるとき' do
            let(:keyword) { '%bag' }
            it_behaves_like '後方一致した商品のみ表示すること'
          end

          context 'キーワードに合致する商品がないとき' do
            let(:keyword) { '%Tote' }
            it_behaves_like '一致する商品がない旨のメッセージを表示すること'
          end
        end

        context '前後方一致検索のとき(%keyword%)' do
          context 'キーワードに合致する商品があるとき' do
            let(:keyword) { '%Shirt%' }
            it_behaves_like 'Shirtのみ表示されていること'
          end

          context 'キーワードに合致する商品がないとき' do
            let(:keyword) { '%Tote%' }
            it_behaves_like '一致する商品がない旨のメッセージを表示すること'
          end
        end
      end
    end

    context '説明文の検索の場合' do
      context 'メタ文字なしの通常検索のとき(keyword)' do
        context 'キーワードに合致する商品があるとき' do
          let(:keyword) { 'quality' }
          it_behaves_like 'Shirtのみ表示されていること'
        end

        context 'キーワードに合致する商品がないとき' do
          let(:keyword) { 'foobar' }
          it_behaves_like '一致する商品がない旨のメッセージを表示すること'
        end
      end

      context 'メタ文字ありの検索のとき' do
        context '前方一致検索のとき(keyword%)' do
          context 'キーワードに合致する商品があるとき' do
            let(:keyword) { 'High%' }
            it_behaves_like '前方一致する商品のみ表示すること'
          end

          context 'キーワードに合致する商品がないとき' do
            let(:keyword) { 'foobar%' }
            it_behaves_like '一致する商品がない旨のメッセージを表示すること'
          end
        end

        context '後方一致検索のとき(keyword%)' do
          context 'キーワードに合致する商品があるとき' do
            let(:keyword) { '%terrible' }
            it_behaves_like '後方一致した商品のみ表示すること'
          end

          context 'キーワードに合致する商品がないとき' do
            let(:keyword) { '%foobar' }
            it_behaves_like '一致する商品がない旨のメッセージを表示すること'
          end
        end

        context '前後方一致検索のとき(%keyword%)' do
          context 'キーワードに合致する商品があるとき' do
            let(:keyword) { '%quality%' }
            it_behaves_like 'Shirtのみ表示されていること'
          end

          context 'キーワードに合致する商品がないとき' do
            let(:keyword) { '%foobar%' }
            it_behaves_like '一致する商品がない旨のメッセージを表示すること'
          end
        end
      end
    end
  end

  describe '色、サイズオプションの検証' do
    include_context 'products_search_setup'

    context '色オプションについて' do
      context '商品に色オプションが存在する場合' do
        before { get potepan_products_path, params: { keyword: 'Shirt' } }
        it '関連する色オプションのみ表示すること' do
          expect(response.body).to     include('Red')
          expect(response.body).to     include('Blue')
          expect(response.body).to_not include('Yellow')
        end
      end

      context '商品に色オプションが存在しない場合' do
        before { get potepan_products_path, params: { keyword: 'Bag' } }
        it '色オプションがない旨のメッセージを表示すること' do
          expect(response.body).to include('色のオプションはありません')
        end
      end
    end

    context 'サイズオプションについて' do
      context '商品にサイズオプションが存在する場合' do
        before { get potepan_products_path, params: { keyword: 'Shirt' } }
        it '関連するサイズオプションのみ表示すること' do
          expect(response.body).to     include('Size-S')
          expect(response.body).to     include('Size-M')
          expect(response.body).to_not include('Size-L')
        end
      end

      context '商品にサイズオプションが存在しない場合' do
        before { get potepan_products_path, params: { keyword: 'Bag' } }
        it 'サイズオプションがない旨のメッセージを表示すること' do
          expect(response.body).to include('サイズのオプションはありません')
        end
      end
    end
  end
end
