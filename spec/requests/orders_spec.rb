require 'rails_helper'
# rubocop:disable Metrics/LineLength
RSpec.describe 'Orders', type: :request do
  include_context 'orders_spec_setup'

  def initialize_cookies(token:)
    cookie_tmp =
      ActionDispatch::Request.new(Rails.application.env_config.deep_dup)
                             .cookie_jar
    cookie_tmp.signed['guest_token'] = token
    cookies['guest_token'] = cookie_tmp['guest_token']
  end

  describe 'カート操作の検証' do
    context 'カートに商品がないとき' do
      before { get potepan_cart_path }

      it 'カートに商品がない旨のメッセージが表示されること' do
        expect(response.body).to include('カートに商品はありません')
      end
    end
    context 'カートに商品があるとき' do
      context '直接カートページにアクセスしたとき' do
        before do
          initialize_cookies(token: 'guest_token')
          get potepan_cart_path
        end

        it 'すでにカートに入れている商品が表示されること' do
          expect(response.body).to     include('Good-Shirt')
          expect(response.body).to     include('Color: Red, Size: Size-S')
          expect(response.body).to     include('Color: Blue, Size: Size-S')
          expect(response.body).to_not include('Color: Red, Size: Size-M')
          expect(response.body).to     include('900.00')
        end

        it 'カートに入っている商品の小計が表示されること' do
          expect(response.body).to include('1,800.00')
        end
      end

      context '商品個別ページから「カートに入れる」をクリックしたとき' do
        before do
          params = { variant_id: bad_shirts_blue_m.id, quantity: 1 }
          post populate_potepan_orders_path, params: params
          follow_redirect!
        end

        it 'その商品がカートに入れられていること' do
          expect(response.body).to include('Bad-Shirt')
          expect(response.body).to include('Color: Blue, Size: Size-M')
        end

        it '小計,消費税,総計が表示されること' do
          expect(response.body).to include('100.00')
          expect(response.body).to include('5.00')
          expect(response.body).to include('105.00')
        end
      end

      context 'DELETEリクエストを送信したとき' do
        before do
          initialize_cookies(token: 'guest_token')
          params = { line_item_id: line_item_red_s.id }
          delete remove_line_item_potepan_order_path(order_in_cart.id), params:
                                                                        params
          get potepan_cart_path
        end

        it 'その商品がカートから削除されていること' do
          expect(response.body).to_not include('Color: Red, Size: Size-S')
          expect(response.body).to     include('Color: Blue, Size: Size-S')
        end

        it '削除した商品の値段を減算した小計,消費税,総計が表示されること' do
          expect(response.body).to include('900.00')
          expect(response.body).to include('45.00')
          expect(response.body).to include('945.00')
        end
      end
    end
  end

  describe '商品個数のアップデート検証' do
    before do
      initialize_cookies(token: 'guest_token')
      get potepan_cart_path
    end

    context '有効なパラメータが送信されたとき' do
      before do
        params = { order: { line_items_attributes:
                            { '0' => { quantity: 99, id: line_item_red_s.id } } },
                   id: order_in_cart.id }
        patch potepan_order_path(order_in_cart), params: params
        follow_redirect!
      end

      it 'カートに入っている商品の個数が更新されること' do
        expect(response.body).to include('99')
      end

      it 'カートの小計、消費税、総計が更新されていること' do
        expect(response.body).to include('90,000.00')
        expect(response.body).to include('4,500.00')
        expect(response.body).to include('94,500.00')
      end

      it '成功した旨のフラッシュメッセージを表示すること' do
        expect(response.body).to include('カートの内容を更新しました')
      end
    end

    context '数量を0で送信したとき' do
      before do
        params = { order: { line_items_attributes:
                            { '0' => { quantity: 0, id: line_item_red_s.id } } },
                   id: order_in_cart.id }
        patch potepan_order_path(order_in_cart), params: params
        follow_redirect!
      end

      it 'その商品が削除されること' do
        expect(response.body).to_not include('Color: Red, Size: Size-S')
        expect(response.body).to     include('Color: Blue, Size: Size-S')
      end

      it '削除した商品の値段を減算した小計,消費税,総計が表示されること' do
        expect(response.body).to include('900.00')
        expect(response.body).to include('45.00')
        expect(response.body).to include('945.00')
      end
    end

    context '無効なパラメータが送信されたとき' do
      shared_examples '失敗した旨のフラッシュメッセージを表示すること' do
        it '失敗した旨のフラッシュメッセージを表示すること' do
          expect(response.body).to include('カートの更新に失敗しました')
        end
      end

      context '数量が不正な値のとき' do
        before do
          params =
            { order: { line_items_attributes:
                        { '0' => { quantity: 'foo', id: line_item_red_s.id } } },
              id: order_in_cart.id }
          patch potepan_order_path(order_in_cart), params: params
          follow_redirect!
        end

        it_behaves_like '失敗した旨のフラッシュメッセージを表示すること'
      end

      context 'ラインアイテムが不正な値のとき' do
        before do
          params =
            { order: { line_items_attributes:
                        { '0' => { quantity: 'foo' } } },
              id: order_in_cart.id }
          patch potepan_order_path(order_in_cart), params: params
          follow_redirect!
        end

        it_behaves_like '失敗した旨のフラッシュメッセージを表示すること'
      end

      context 'オーダーIDが不正な値のとき' do
        before do
          params =
            { order: { line_items_attributes:
                        { '0' => { quantity: 'foo' } } },
              id: other_order_in_cart.id }
          patch potepan_order_path(order_in_cart), params: params
          follow_redirect!
        end

        it_behaves_like '失敗した旨のフラッシュメッセージを表示すること'
      end
    end
  end
end
# rubocop:enable Metrics/LineLength
