class Potepan::HomeController < ApplicationController
  def index
    @new_arrival_products = Spree::Product.new_arrival_from(1.week.ago)
    @popular_taxons = Spree::Taxon.popular_taxons
  end
end
