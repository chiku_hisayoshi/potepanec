# https://github.com/solidusio/solidus/blob/master/frontend/app/controllers/spree/orders_controller.rb
require 'spree/core/controller_helpers/order'
require 'spree/core/controller_helpers/store'
require 'spree/core/controller_helpers/auth.rb'

class Potepan::OrdersController < ApplicationController
  include Spree::Core::ControllerHelpers::Order
  include Spree::Core::ControllerHelpers::Store
  include Spree::Core::ControllerHelpers::Auth
  include Constants::DefaultSize

  before_action :set_order_and_line_items, only: %i[edit remove_line_item]
  before_action :assigns_order, only: :update

  def edit; end

  def update
    if @order.contents.update_cart(order_params)
      flash[:success] = 'カートの内容を更新しました'
    else
      flash[:danger] = 'カートの更新に失敗しました'
    end
    redirect_to potepan_cart_path
  end

  def populate
    @order = current_order(create_order_if_necessary: true)
    variant = Spree::Variant.find_by!(id: params[:variant_id])
    quantity =
      params[:quantity].present? ? params[:quantity].to_i : MIN_ORDER_QUANTITY
    @order.contents.add(variant, quantity)
    redirect_to potepan_cart_path
  end

  def remove_line_item
    line_item = @line_items.find_by!(id: params[:line_item_id])
    @order.contents.remove_line_item(line_item)
    redirect_to potepan_cart_path
  end

  private

  def order_params
    params.require(:order).permit(line_items_attributes: %i[quantity id])
  end

  def assigns_order
    @order =
      current_order ||
      Spree::Order.incomplete.find_or_initialize_by(
        guest_token: cookies.signed[:guest_token]
      )
    return if @order

    redirect_to potepan_root_path
  end

  def set_order_and_line_items
    @order =
      current_order ||
      Spree::Order.incomplete.find_or_initialize_by(
        guest_token: cookies.signed[:guest_token]
      )
    @line_items = @order.line_items.includes(variant: :product)
  end
end
