class Potepan::CategoriesController < ApplicationController
  before_action :query_params

  def show
    @taxon = Spree::Taxon.find_by!(id: params[:id])
    @products =
      @taxon.products_sort_and_filter_by(color: @filter_color,
                                         size: @filter_size,
                                         sort_type: params[:sort_type])
    respond_to do |format|
      format.html do
        @taxonomies = Spree::Taxonomy.all.includes(:root)
        @taxons = extract_taxons_from_taxonomies(@taxonomies)
        @filter_colors =
          Spree::OptionValue.filter_option_values_by(:color, @taxon.products)
        @filter_sizes  =
          Spree::OptionValue.filter_option_values_by(:size, @taxon.products)
      end
      format.js
    end
  end

  private

  def query_params
    @display_style = params[:display_style] == 'list' ? 'list' : 'grid'
    @filter_color  = params[:filter_color]
    @filter_size   = params[:filter_size]
  end

  def extract_taxons_from_taxonomies(taxonomies)
    taxonomies.map do |taxonomy|
      [taxonomy.id,
       Spree::Taxon.where(id: taxonomy.taxons.ids).includes(:children)]
    end.to_h
  end
end
