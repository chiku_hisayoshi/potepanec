class Potepan::ProductsController < ApplicationController
  before_action :query_params
  before_action :redirect_to_root_when_keyword_blank, only: :index

  def index
    @products =
      Spree::Product.search_and_sort_and_filter_by(
        keyword: @keyword,
        color: @filter_color,
        size: @filter_size
      )
    @ordered_products =
      Spree::Product.custom_order_products(
        products: @products,
        sort_type: params[:sort_type]
      )
    respond_to do |format|
      format.html do
        @filter_colors =
          Spree::OptionValue.filter_option_values_by(:color, @products)
        @filter_sizes  =
          Spree::OptionValue.filter_option_values_by(:size, @products)
      end
      format.js
    end
  end

  def show
    @product = Spree::Product.find_by!(slug: params[:id])
    @variant = @product.variants.find_by(id: params[:variant_id]) ||
               @product.variants.first || @product
    @images  = @variant.images
    respond_to do |format|
      format.html { @similar_products = @product.extract_similar_products }
      format.js
    end
  end

  private

  def redirect_to_root_when_keyword_blank
    redirect_to potepan_root_path if params[:keyword].blank?
  end

  def query_params
    @display_style = params[:display_style] == 'list' ? 'list' : 'grid'
    @filter_color  = params[:filter_color]
    @filter_size   = params[:filter_size]
    @keyword       = params[:keyword]
  end
end
