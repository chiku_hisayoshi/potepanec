module Potepan::ProductDecorator
  include Constants::DefaultSize
  include Constants::SortType

  # クラスレベル ================
  def self.prepended(base)
    def base.new_arrival_from(from)
      where('available_on BETWEEN ? AND ?', from, Time.zone.now)
        .order(available_on: :desc)
        .includes(master: %i[images default_price])
    end

    def base.search_and_sort_and_filter_by(keyword:, color:, size:)
      includes(:variants, master: %i[images default_price])
        .eager_load(variants: %i[option_values_variants prices])
        .where(where_sentence(keyword, color, size))
        .distinct
    end

    def base.where_sentence(keyword, color, size)
      where_hash_base =
        { spree_products: { id: extract_products_from_keyword(keyword).ids } }
      return where_hash_base if color.nil? && size.nil?

      options = []
      options << color if color
      options << size  if size
      filterd_variant_ids =
        Spree::OptionValuesVariant
        .select(:variant_id)
        .where(option_value_id: options)
        .group(:variant_id)
        .having('COUNT(*) = ?', options.size)
      where_hash_base.merge!(
        spree_option_values_variants: { variant_id: filterd_variant_ids }
      )
    end

    def base.extract_products_from_keyword(keyword)
      meta_keyword =
        if keyword.start_with?('%') || keyword.end_with?('%')
          keyword
        else
          "%#{keyword}%"
        end

      where('name LIKE ? OR description LIKE ?', meta_keyword, meta_keyword)
    end

    def base.custom_order_products(products:, sort_type:)
      products.order(order_sentence(sort_type))
    end

    def base.order_sentence(sort_type = NEW_ARRIVAL)
      case sort_type.to_i
      when NEW_ARRIVAL
        'spree_products.available_on DESC'
      when PRICE_ASC
        'default_prices_spree_variants.amount ASC'
      when PRICE_DESC
        'default_prices_spree_variants.amount DESC'
      when OLDDER
        'spree_products.available_on ASC'
      else # フェールセーフ
        'spree_products.available_on DESC'
      end
    end
  end

  # インスタンスレベル ================
  def extract_similar_products
    return [] if taxon_ids.empty?

    Spree::Product.joins(:taxons)
                  .includes(master: %i[images default_price])
                  .where('spree_taxons.id IN (?) AND spree_products.id <> ?',
                         taxon_ids, id)
                  .distinct
                  .sample(MAX_SIMILAR_PRODUCTS)
  end

  Spree::Product.prepend self
end
