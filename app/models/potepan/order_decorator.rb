module Potepan::OrderDecorator
  # インスタンスレベル ================
  def display_tax_amount
    Spree::Money.new(adjustment_total, currency: currency)
  end

  Spree::Order.prepend self
end
