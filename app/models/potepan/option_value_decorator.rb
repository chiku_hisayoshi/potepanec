module Potepan::OptionValueDecorator
  def self.prepended(base)
    def base.filter_option_values_by(option_name, products)
      return [] if products.empty?

      joins(:option_type, option_values_variants: :variant)
        .where(
          'spree_option_types.name LIKE ? AND spree_variants.product_id IN (?)',
          "%#{option_name}%", products.ids
        )
        .distinct
    end
  end

  Spree::OptionValue.prepend self
end
