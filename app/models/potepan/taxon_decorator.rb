module Potepan::TaxonDecorator
  include Constants::DefaultSize
  include Constants::SortType

  # クラスレベル ================
  def self.prepended(base)
    def base.popular_taxons
      parent_ids = joins(:children).distinct.ids
      joins(:products).where.not(spree_taxons: { id: parent_ids })
                      .distinct
                      .sample(MAX_POPULAR_CATEGORY_PRODUCTS)
    end
  end

  # インスタンスレベル ================
  def products_sort_and_filter_by(color:, size:, sort_type:)
    Spree::Product
      .includes(:variants, master: %i[images default_price])
      .eager_load(variants: %i[option_values_variants prices])
      .where(where_sentence(color, size))
      .distinct
      .order(order_sentence(sort_type))
  end

  def where_sentence(color, size)
    where_hash_base = { spree_products: { id: product_ids } }
    return where_hash_base if color.nil? && size.nil?

    options = []
    options << color if color
    options << size  if size

    filterd_variant_ids =
      Spree::OptionValuesVariant
      .select(:variant_id)
      .where(option_value_id: options)
      .group(:variant_id)
      .having('COUNT(*) = ?', options.size)

    where_hash_base.merge!(
      spree_option_values_variants: { variant_id: filterd_variant_ids }
    )
  end

  def order_sentence(sort_type = NEW_ARRIVAL)
    case sort_type.to_i
    when NEW_ARRIVAL
      'spree_products.available_on DESC'
    when PRICE_ASC
      'default_prices_spree_variants.amount ASC'
    when PRICE_DESC
      'default_prices_spree_variants.amount DESC'
    when OLDDER
      'spree_products.available_on ASC'
    else # フェールセーフ
      'spree_products.available_on DESC'
    end
  end

  Spree::Taxon.prepend self
end
