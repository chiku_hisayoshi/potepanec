module CategoriesHelper
  def taxon_list(taxon, display_style)
    return '' if taxon.children.present?

    tag.li do
      link_to potepan_category_path(taxon.id, display_style: display_style) do
        tag.i(class: 'fa fa-caret-right', aria: { hidden: true }) <<
          tag.span("#{taxon.name} ( #{taxon.products.size} )")
      end
    end
  end

  def variation_message(filter_color:, filter_size:)
    return 'バリエーションあり' if filter_color.nil? && filter_size.nil?

    '条件に合致するバリエーションあり'
  end

  def filter_query_params(filter_color:, filter_size:, display_style:, selected_color:, selected_size:, keyword:) # rubocop:disable Metrics/LineLength
    base_params = { display_style: display_style, keyword: keyword }
    return base_params if filter_color.nil? && filter_size.nil?

    if filter_color.present? && filter_size.present?
      if selected_color
        base_params.merge(filter_size: filter_size)
      elsif selected_size
        base_params.merge(filter_color: filter_color)
      else
        base_params.merge(filter_size: filter_size, filter_color: filter_color)
      end
    elsif filter_color.present?
      if selected_color
        base_params
      else
        base_params.merge(filter_color: filter_color)
      end
    elsif filter_size.present?
      if selected_size
        base_params
      else
        base_params.merge(filter_size: filter_size)
      end
    end
  end
end
