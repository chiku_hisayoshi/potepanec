module ApplicationHelper
  def full_page_title(title = '')
    base_title = 'Potepanec'
    return base_title if title.blank?

    "#{title} - #{base_title}"
  end
end
