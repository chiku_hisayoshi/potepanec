module ProductsHelper
  def amount_stock_items(variant)
    variant.stock_items.inject(0) do |result, stock_item|
      result + stock_item.count_on_hand
    end
  end

  def order_quantity_options(max_quantity:)
    quantity = []
    max_quantity.times do |index|
      quantity_num = index + 1
      quantity << [quantity_num, quantity_num]
    end
    quantity
  end
end
